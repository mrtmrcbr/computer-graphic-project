#include <GL/glut.h>
#include <cstdio>

const unsigned int window_position_x = 50;
const unsigned int window_position_y = 50;
const unsigned int window_width = 480;
const unsigned int window_height = 640;

const float enemy_square_size = 10.0;

float enemy_1_movement_size = 10.0;
float enemy_2_movement_size = 10.0;
float enemy_3_movement_size = 10.0;
float enemy_4_movement_size = 10.0;

float enemy_1_move_at_x;
float enemy_2_move_at_x;
float enemy_3_move_at_x;
float enemy_4_move_at_x;

float enemy_1_move_at_y;
float enemy_2_move_at_y;
float enemy_3_move_at_y;
float enemy_4_move_at_y;
float speed = 10;

const float master_square_size = 10.0;
float master_movement_size = 10.0;
float master_ship_move_x;
float master_ship_move_y;

bool life_toogle = false;
int life_point = 3;
unsigned int global_score = 0;
const unsigned int score = 10;

void init_game(){
	enemy_1_move_at_x = -90.0;	
	enemy_2_move_at_x = -90.0;
	enemy_3_move_at_x = -90.0;
	enemy_4_move_at_x = -90.0;	
	
	enemy_1_move_at_y = 100.0;
	enemy_2_move_at_y = 50.0;	
	enemy_3_move_at_y = 0.0;
	enemy_4_move_at_y = 75.0;

	master_ship_move_x = 0.0;
	master_ship_move_y = -100.0;
}

void update_enemy_speeds(float speed = 0){
}

void reshape(GLint w, GLint h) {
    glViewport(0, 0, w, h);
    GLfloat aspect = (GLfloat)w / (GLfloat)h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-100.0, 100.0, -100.0/aspect, 100.0/aspect, -1.0, 1.0);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    glTranslatef(enemy_1_move_at_x,enemy_1_move_at_y,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(enemy_2_move_at_x,enemy_2_move_at_y,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(enemy_3_move_at_x,enemy_3_move_at_y,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(enemy_4_move_at_x,enemy_4_move_at_y,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(master_ship_move_x,master_ship_move_y,0.0);
    glRectf(-master_square_size,-master_square_size,master_square_size, master_square_size);
    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}

void timer(int v) {
	if(enemy_1_move_at_x - enemy_square_size <-100 || enemy_1_move_at_x + enemy_square_size >100){
		enemy_1_movement_size *= -1;
	}
	enemy_1_move_at_x = enemy_1_move_at_x + enemy_1_movement_size;

	if(enemy_2_move_at_x - enemy_square_size <-100 || enemy_2_move_at_x + enemy_square_size >100 ){
   		enemy_2_movement_size *= -1;
	}
	enemy_2_move_at_x = enemy_2_move_at_x + enemy_2_movement_size;

	if(enemy_3_move_at_x - enemy_square_size <-100 || enemy_3_move_at_x + enemy_square_size >100 ){
		enemy_3_movement_size *= -1;
	}
	enemy_3_move_at_x = enemy_3_move_at_x + enemy_3_movement_size;

	if(enemy_4_move_at_x - enemy_square_size <-100 || enemy_4_move_at_x + enemy_square_size >100 ){
		enemy_4_movement_size *= -1;
	}
	enemy_4_move_at_x = enemy_4_move_at_x + enemy_4_movement_size;

	if(
		enemy_1_move_at_x <= master_ship_move_x && master_ship_move_x <= enemy_1_move_at_x + enemy_square_size &&
		enemy_1_move_at_y >= master_ship_move_y && master_ship_move_y >= enemy_1_move_at_y - enemy_square_size
	){
		life_toogle = true;
	}
	if(
		enemy_2_move_at_x <= master_ship_move_x && master_ship_move_x <= enemy_2_move_at_x + enemy_square_size &&
		enemy_2_move_at_y >= master_ship_move_y && master_ship_move_y >= enemy_2_move_at_y - enemy_square_size
	){
		life_toogle = true;
	}
	if(
		enemy_3_move_at_x <= master_ship_move_x && master_ship_move_x <= enemy_3_move_at_x + enemy_square_size &&
		enemy_3_move_at_y >= master_ship_move_y && master_ship_move_y >= enemy_3_move_at_y - enemy_square_size
	){
		life_toogle = true;
	}
	if(
		enemy_4_move_at_x <= master_ship_move_x && master_ship_move_x <= enemy_4_move_at_x + enemy_square_size &&
		enemy_4_move_at_y >= master_ship_move_y && master_ship_move_y >= enemy_4_move_at_y - enemy_square_size
	){
		life_toogle = true;
	}


	if(life_toogle){
		if(life_point >= 0){
			printf("Decimals: %d \n", life_point);
			//life_point -= 1;	
			life_toogle = false;
			init_game();
		}else{
		
		}
	}
	glutPostRedisplay();
	glutTimerFunc(100, timer, v);
}

void special_keys(int key, int x, int y) {
	if(key == GLUT_KEY_RIGHT){
		if(master_ship_move_x <= 100 - master_square_size){
			master_ship_move_x += master_movement_size;
		}
	}
        if(key == GLUT_KEY_LEFT){
		if(master_ship_move_x >= -100 + master_square_size){
			master_ship_move_x -= master_movement_size;
		}
	}
        if(key == GLUT_KEY_UP){
		if(master_ship_move_y <= 100 + master_square_size*2){
			master_ship_move_y += master_movement_size;
		}

		if(master_ship_move_y >= 100 + master_square_size*3)
		{
			global_score += score;

			enemy_1_movement_size += speed;
			enemy_2_movement_size += speed;
			enemy_3_movement_size += speed;	
			enemy_4_movement_size += speed;	

			printf("score: %d \n", global_score);
			printf("speed: %d \n", enemy_1_movement_size);

			init_game();
		}
	}
        if(key == GLUT_KEY_DOWN){
		if(master_ship_move_y >= -100 - master_square_size*2){
			master_ship_move_y -= master_movement_size;
		}
	}

        glutPostRedisplay();
    
}

int main(int argc, char** argv) {
	init_game();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition(window_position_x, window_position_y);
    glutInitWindowSize(window_width, window_height);
    glutCreateWindow("rat");
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutSpecialFunc(special_keys);
    glutTimerFunc(100, timer, 0);
    glutMainLoop();
}
