#include <GL/glut.h>

unsigned int window_position_x = 50;
unsigned int window_position_y = 50;
unsigned int window_width = 480;
unsigned int window_height = 640;

float enemy_square_size = 10.0;
float enemy_move_at_x = -100.0;
float enemy_movement_size = 10.0;

float master_square_size = 10.0;
float master_movement_size = 10.0;
float master_ship_move_y = -100.0;
float master_ship_move_x = 0.0;

void reshape(GLint w, GLint h) {
    glViewport(0, 0, w, h);
    GLfloat aspect = (GLfloat)w / (GLfloat)h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-100.0, 100.0, -100.0/aspect, 100.0/aspect, -1.0, 1.0);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    glTranslatef(enemy_move_at_x,100.0,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-enemy_move_at_x,50.0,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(enemy_move_at_x,0.0,0.0);
    glRectf(-enemy_square_size,-enemy_square_size,enemy_square_size, enemy_square_size);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(master_ship_move_x,master_ship_move_y,0.0);
    glRectf(-master_square_size,-master_square_size,master_square_size, master_square_size);
    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}

void timer(int v) {

    if(enemy_move_at_x>100 || enemy_move_at_x<-100){
        enemy_movement_size *= -1;
    }

    enemy_move_at_x = enemy_move_at_x + enemy_movement_size;

    glutPostRedisplay();
    glutTimerFunc(100, timer, v);
}

void special_keys(int key, int x, int y) {

        if(key == GLUT_KEY_RIGHT)
            master_ship_move_x += master_movement_size;
        else if(key == GLUT_KEY_LEFT)
            master_ship_move_x -= master_movement_size;
        else if(key == GLUT_KEY_UP)
            master_ship_move_y += master_movement_size;
        else if(key == GLUT_KEY_DOWN)
            master_ship_move_y -= master_movement_size;

        glutPostRedisplay();
    
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition(window_position_x, window_position_y);
    glutInitWindowSize(window_width, window_height);
    glutCreateWindow("rat");
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutSpecialFunc(special_keys);
    glutTimerFunc(100, timer, 0);
    glutMainLoop();
}